/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat7;

/**
 *
 * @author batoi
 */
public class TestPersona {
     public static void main(String[] args) {
        Persona persona = new Persona();
        PersonaInglesa personaInglesa = new PersonaInglesa("ABCDE");
        PersonaRusa personaRusa = new PersonaRusa();
        PersonaFrancesa personaFrancesa = new PersonaFrancesa();
        
        persona.saluda();
        personaInglesa.saluda();
        personaRusa.saluda();
        personaFrancesa.saluda();
    }
}
