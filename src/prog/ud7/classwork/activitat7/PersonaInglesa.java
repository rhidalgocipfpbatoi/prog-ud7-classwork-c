/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat7;

/**
 *
 * @author batoi
 */
public class PersonaInglesa extends Persona {
    
    private String pasaporte;

    public PersonaInglesa(String pasaporte) {
        this.pasaporte = pasaporte;
    }
    
    @Override
    public void saluda() {
        System.out.println("Hi! I'm an english person");
    }
    
    public void mostrarPasaporte() {
        System.out.println("My passport is " + pasaporte);
    }
}
