/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat7;

/**
 *
 * @author batoi
 */
public class TestAduana {
    
    public static void main(String[] args) {
        
        Aduana aduana = new Aduana();
        
        Persona persona = new Persona();
        PersonaInglesa personaInglesa = new PersonaInglesa("PSC123123");
        PersonaRusa personaRusa = new PersonaRusa();
        PersonaFrancesa personaFrancesa = new PersonaFrancesa();
        
        aduana.entrar(persona);
        aduana.entrar(personaInglesa);
        aduana.entrar(personaRusa);
        aduana.entrar(personaFrancesa);
    }
    
}
