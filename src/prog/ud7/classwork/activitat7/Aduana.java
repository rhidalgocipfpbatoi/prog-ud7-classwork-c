/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat7;

/**
 *
 * @author batoi
 */
public class Aduana {
    
    private Persona[] personas;
    
    public Aduana() {
        this.personas = new Persona[10];
    }
    
    public void entrar(Persona persona) {
        
        System.out.println("You're welcome");
        persona.saluda();
        
        if (persona instanceof PersonaInglesa) {
            System.out.println("Could you show me your passport, please?");
            ((PersonaInglesa) persona).mostrarPasaporte();
        }
            
        for (int i = 0; i < personas.length; i++) {
            if (personas[i] == null) {
                personas[i] = persona;
                System.out.println("You can go inside the queue");
                return;
            }
        }
        
        System.out.println("You can't go into the queue");
    }
}
