package prog.ud7.classwork.actividad10;

import prog.ud7.classwork.actividad10.enums.Food;
import prog.ud7.classwork.actividad10.enums.Size;
import actividad10.types.*;
import prog.ud7.classwork.actividad10.types.Animal;
import prog.ud7.classwork.actividad10.types.Cat;
import prog.ud7.classwork.actividad10.types.Dog;
import prog.ud7.classwork.actividad10.types.Hippo;
import prog.ud7.classwork.actividad10.types.Horse;
import prog.ud7.classwork.actividad10.types.Lion;
import prog.ud7.classwork.actividad10.types.Tiger;
import prog.ud7.classwork.actividad10.types.Wolf;

public class TestAnimal {

    public static void main(String[] args) {

        Animal[] animals = new Animal[10];
        animals[0] = new Lion(Size.BIG, "La Sabana");
        animals[1] = new Wolf(Size.BIG, "La Sabana");
        animals[2] = new Wolf(Size.MEDIUM, "La Serreta");
        animals[3] = new Dog(Size.SMALL, "Alcoy");
        animals[4] = new Cat(Size.SMALL, "Spain");
        animals[5] = new Cat(Size.SMALL, "Spain");
        animals[6] = new Horse(Size.BIG, "Spain");
        animals[7] = new Hippo(Size.BIG, "La Sabana");
        animals[8] = new Tiger(Size.BIG, "África");
        animals[9] = new Tiger(Size.BIG, "África");

        // Actividad 10
        System.out.println(animals[3].equals(animals[4])); // --> no son iguales por ser ambos pequeños pero uno es omnivoros y el otro carnívoro
        System.out.println(animals[0].equals(animals[9])); // --> son iguales por ser ambos grandes y carnívoros
    }
}
