package prog.ud7.classwork.actividad10.enums;

public enum Size {
    BIG, MEDIUM, SMALL
};