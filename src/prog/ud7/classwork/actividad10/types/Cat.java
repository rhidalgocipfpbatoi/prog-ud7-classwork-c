package prog.ud7.classwork.actividad10.types;


import prog.ud7.classwork.actividad10.enums.Food;
import prog.ud7.classwork.actividad10.enums.Size;


public class Cat extends Animal {

    private static String NOMBRE = "Gato";
    public Cat(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("Miauuuu!!!!!!!!!");
    }
}
