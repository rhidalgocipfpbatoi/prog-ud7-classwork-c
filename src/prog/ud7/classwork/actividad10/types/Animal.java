package prog.ud7.classwork.actividad10.types;

import prog.ud7.classwork.actividad10.enums.Food;
import prog.ud7.classwork.actividad10.enums.Size;



public abstract class Animal {

    private Food food;

    private Size size;

    private String location;

    private int hunger;

    private boolean vaccinated;

    public Animal(Food food, Size size, String location) {
        this.food = food;
        this.size = size;
        this.location = location;
        this.hunger = 8;
        this.vaccinated = false;
    }

    public void eat() {
        hunger -= hungerLevel();
    }

    private int hungerLevel() {
        switch (this.food) {
            case OMNIVORE: return 3;
            case CARNIVOROUS: return 1;
            default: return 2;
        }
    }

    public void vaccinate() {
        System.out.println("Vacunando a un " + getNombre().toLowerCase() + "...");
        vaccinated = true;
        makeNoise();
    }

    protected abstract String getNombre();

    public abstract void makeNoise();

    @Override
    public String toString() {
        return  getNombre() + ": Tamaño = " + size + ", Nivel de hambre = "
                + hunger + ", vacunado = " + vaccinated + "  vive en = " + location;
    }

    @Override
    public boolean equals(Object otro) {
        if (!(otro instanceof  Animal)) {
            return false;
        }

        Animal otroAnimal = ((Animal) otro);
        return this.food == otroAnimal.food && this.size == otroAnimal.size;
    }
}
