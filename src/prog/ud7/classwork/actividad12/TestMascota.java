/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.actividad12;

import prog.ud7.classwork.actividad12.enums.Size;
import prog.ud7.classwork.actividad12.types.mascotas.Cat;
import prog.ud7.classwork.actividad12.types.mascotas.Dog;
import prog.ud7.classwork.actividad12.types.mascotas.Pet;
import prog.ud7.classwork.actividad12.types.mascotas.RoboDog;

/**
 *
 * @author batoi
 */
public class TestMascota {
    public static void main(String[] args) {
        Persona persona = new Persona();
        
        Pet[] pets = new Pet[3];
        
        pets[0] = new RoboDog();
        pets[1] = new Dog(Size.SMALL, "Mi casa");
        pets[2] = new Cat(Size.SMALL, "Tu casa");
        
        for (Pet pet : pets) {
            persona.interactuar(pet);
        }
    }
}
