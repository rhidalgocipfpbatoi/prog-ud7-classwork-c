package prog.ud7.classwork.actividad12.enums;

public enum Size {
    BIG, MEDIUM, SMALL
};