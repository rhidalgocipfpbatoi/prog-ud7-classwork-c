/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.actividad12.types.mascotas;

import prog.ud7.classwork.actividad12.types.mascotas.Pet;

/**
 *
 * @author batoi
 */
public class RoboDog implements Pet {

    @Override
    public void beFriendly() {
        System.out.println("Moviendo la cabeza");
    }

    @Override
    public void play() {
        System.out.println("Siguiendo una línea");
    }

    @Override
    public void getGreeting() {
        System.out.println("Encendiendo el robot");
    }
    
}
