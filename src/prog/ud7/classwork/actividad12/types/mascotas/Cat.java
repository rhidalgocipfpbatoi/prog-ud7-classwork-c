package prog.ud7.classwork.actividad12.types.mascotas;

import prog.ud7.classwork.actividad12.types.mascotas.Pet;
import prog.ud7.classwork.actividad12.enums.Food;
import prog.ud7.classwork.actividad12.enums.Size;
import prog.ud7.classwork.actividad12.types.Animal;


public class Cat extends Animal implements Pet {

    private static String NOMBRE = "Gato";
    
    public Cat(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("Miauuuu!!!!!!!!!");
    }

    @Override
    public void beFriendly() {
        System.out.println("Ronronea");
    }

    @Override
    public void play() {
        System.out.println("Jugando con el ovillo de lana");
    }

    @Override
    public void getGreeting() {
        System.out.println("Hola gatito");
    }

    
}
