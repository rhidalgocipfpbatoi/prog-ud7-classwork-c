package prog.ud7.classwork.actividad12.types.mascotas;

import prog.ud7.classwork.actividad12.types.mascotas.Pet;
import prog.ud7.classwork.actividad12.enums.Food;
import prog.ud7.classwork.actividad12.enums.Size;
import prog.ud7.classwork.actividad12.types.Animal;

public class Dog extends Animal implements Pet {

    private static String NOMBRE = "Perro";

    public Dog(Size size, String location) {
        super(Food.OMNIVORE, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("GUAU!!!");
    }

    @Override
    public void beFriendly() {
        System.out.println("Moviendo la cola");
    }

    @Override
    public void play() {
        System.out.println("Jugando con el frisbee");
    }

    @Override
    public void getGreeting() {
        System.out.println("Hola perrito");
    }
}
