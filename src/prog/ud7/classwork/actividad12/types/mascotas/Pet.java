/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package prog.ud7.classwork.actividad12.types.mascotas;

/**
 *
 * @author batoi
 */
public interface Pet {
    void beFriendly();
    void play();
    void getGreeting();
}
