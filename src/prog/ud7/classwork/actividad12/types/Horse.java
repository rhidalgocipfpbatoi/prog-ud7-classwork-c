package prog.ud7.classwork.actividad12.types;

import prog.ud7.classwork.actividad12.enums.Food;
import prog.ud7.classwork.actividad12.enums.Size;

public class Horse extends Animal {

    private static String NOMBRE = "Caballo";

    public Horse(Size size, String location) {
        super(Food.HERBIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("IIIIIIH....!");
    }
}
