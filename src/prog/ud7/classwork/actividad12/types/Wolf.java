package prog.ud7.classwork.actividad12.types;

import prog.ud7.classwork.actividad12.enums.Food;
import prog.ud7.classwork.actividad12.enums.Size;


public class Wolf extends Animal {

    private static final String NOMBRE = "Lobo";
    public Wolf(Size size, String location) {
        super(Food.OMNIVORE, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("AUUUUUUUHHHH!!!!!!!!!");
    }
}
