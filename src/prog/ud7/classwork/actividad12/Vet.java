package prog.ud7.classwork.actividad12;

import prog.ud7.classwork.actividad12.types.*;



public class Vet {

    public void vaccinate(Animal animal) {
        if (animal instanceof Lion
                || animal instanceof Tiger) {
            animal.eat();
        }
        animal.vaccinate();
    }

    public void feed(Animal animal) {
        animal.eat();
    }

}
