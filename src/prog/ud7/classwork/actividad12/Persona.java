/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.actividad12;

import prog.ud7.classwork.actividad12.types.mascotas.Cat;
import prog.ud7.classwork.actividad12.types.mascotas.Dog;
import prog.ud7.classwork.actividad12.types.mascotas.Pet;


/**
 *
 * @author batoi
 */
public class Persona {
    
    private String DNI;
    
    public void saluda() {
        System.out.println("Hola soy una persona");
    }

    public String getDNI() {
        return DNI;
    }
    
    public void interactuar(Pet mascota) {
        
        mascota.getGreeting();
        mascota.beFriendly();
        mascota.play();
    }
    
    
}
