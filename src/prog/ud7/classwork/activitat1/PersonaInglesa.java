/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat1;

import prog.ud7.classwork.activitat7.Persona;

/**
 *
 * @author batoi
 */
public class PersonaInglesa extends Persona {
    
    private String DNI;
    
    @Override
    public void saluda() {
        System.out.println("Hi! I'm an english person");
    }
}
