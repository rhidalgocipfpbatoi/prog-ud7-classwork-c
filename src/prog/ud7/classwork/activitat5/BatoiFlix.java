package prog.ud7.classwork.activitat5;

public class BatoiFlix {

    public static void main(String[] args) {

        Fecha releaseDate = new Fecha("19/02/2021");
        Documental dreamSongs = new Documental("Dream Songs", 2400l, Formato.MPG, releaseDate, "Enrique Juncosa",
                "Movimiento Hippie");
        dreamSongs.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        releaseDate = new Fecha("20/06/2020");
        Pelicula newsOfTheWorld = new Pelicula("News of the World", 7200l, Formato.AVI, releaseDate, "Tom Hanks",
                "Carolina Betller");
        newsOfTheWorld.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        Fecha date = new Fecha("04/06/2001");
        Serie elHacker = new Serie("El hacker", 3600l, Formato.FLV, date, "Alejandro Macci",
                1, 20);
        elHacker.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        System.out.println("\nListado Producciones (Uso toString()) \n");
        System.out.println(newsOfTheWorld);
        System.out.println(dreamSongs);
        System.out.println(elHacker);

        System.out.println("\nDetalle Producciones (Uso mostrarDetalle())\n");
        newsOfTheWorld.showDetails();
        dreamSongs.showDetails();
        elHacker.showDetails();

    }

}