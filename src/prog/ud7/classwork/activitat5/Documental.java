package prog.ud7.classwork.activitat5;

public class Documental extends Produccion {

    private String investigador;

    private String tema;

    public Documental(String title, long duration, Formato format, Fecha releaseDate, String investigador, String tema) {
        super(title, duration, format, releaseDate);
        this.investigador = investigador;
        this.tema = tema;
    }

    public Documental(String title, Formato format, Fecha releaseDate, String investigador, String tema) {
        super(title, 2000l, format, releaseDate);
        this.investigador = investigador;
        this.tema = tema;
    }

    @Override
    public void showDetails() {
        System.out.println("---------------- Documental ---------------");
        super.showDetails();
        System.out.println("Investigador: " + investigador);
        System.out.println("Duración: " + getDurationTime());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Documental)" + " investigador= " + investigador + ", tema= " + tema;
    }

}
