package prog.ud7.classwork.activitat5;

public class Serie extends Produccion {

    private int season;

    private int chapter;

    public Serie(String title, long duration, Formato format, Fecha releaseDate, String director, int season, int chapter) {
        super(title, duration, format, releaseDate);
        this.season = season;
        this.chapter = chapter;
    }

    public Serie(String title, Formato format, Fecha releaseDate, String director, int season, int chapter) {
        super(title, 2400l, format, releaseDate);
        this.season = season;
        this.chapter = chapter;
    }

    public void showDetails() {
        System.out.println("-------------------Serie--------------------");
        super.showDetails();
        System.out.printf("Temporada %d - Capítulos: %d %n", chapter, season);
        System.out.println("Duración: " + getDurationTime());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Serie) temporada = " + season + ", capítulos=" + chapter;
    }

}
