package prog.ud7.classwork.activitat5;

public class Pelicula extends Produccion {

    private String mainActor;

    private String mainActress;

    public Pelicula(String title, long duration, Formato format, Fecha releaseDate, String mainActor) {
        super(title, duration, format, releaseDate);
        this.mainActor = mainActor;
    }

    public Pelicula(String mainActress, String title, long duration, Formato format, Fecha releaseDate) {
        super(title, duration, format, releaseDate);
        this.mainActress = mainActress;
    }

    public Pelicula(String title, long duration, Formato format, Fecha releaseDate, String mainActor, String mainActress) {
        super(title, duration, format, releaseDate);
        this.mainActor = mainActor;
        this.mainActress = mainActress;
    }

    public Pelicula(String title, Formato format, Fecha releaseDate, String mainActor, String mainActress) {
        super(title, 4800l, format, releaseDate);
        this.mainActor = mainActor;
        this.mainActress = mainActress;
    }

    @Override
    public void showDetails() {
        System.out.println("----------------- Película ----------------");
        super.showDetails();
        System.out.println("Duración: " + getDurationTime());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Película)" + ", actor: = " + mainActor + ", actriz= " + mainActress;
    }
}
