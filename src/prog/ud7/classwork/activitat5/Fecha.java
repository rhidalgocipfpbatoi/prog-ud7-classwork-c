package prog.ud7.classwork.activitat5;

import java.util.StringTokenizer;

public class Fecha {

    private int dia;
    private int mes;
    private int anyo;

    private static final int MAX_DIAS_ANYADIR_O_RESTAR = 30;
    private static final int DIAS_POR_ANYO = 365;

    private static final String[] DIAS_TEXTO = new String[] { "domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"};

    private static final String[] MESES_TEXTO = new String[] { "enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

    private static final int MESES_POR_ANYO = MESES_TEXTO.length;
    private static final int DIAS_POR_SEMANA = DIAS_TEXTO.length;

    /**
     *  Constructor por defecto
     *  Inicializa una fecha a dia 1-1-1
     */
    public Fecha() {
        this(1, 1, 1);
    }

    /**
     *  Inicializa la fecha a partir de los parámetros recibidos
     */
    public Fecha(int dia, int mes, int anyo) {
        this.set(dia, mes, anyo);
    }


    /**
     * Inicializa la fecha a partir de otra pasada en formato String dd/mm/yyyy
     *
     * Deberemos trocearlas de forma que asignemos el día més y año a cada uno de los atributoe
     * @param fecha
     */
    public Fecha(String fecha) {
        StringTokenizer stringTokenizer = new StringTokenizer(fecha, "/");
        this.set(Integer.parseInt(stringTokenizer.nextToken()),
                Integer.parseInt(stringTokenizer.nextToken()),
                Integer.parseInt(stringTokenizer.nextToken()));
    }

    /**
     * Modifica la fecha actual a partir de los datos pasados como argumento
     */
    public void set(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    /**
     * Creará y devolverá un nuevo objeto de la clase Fecha iniciado
     * con los valores que representa el objeto actual, es decir creará un clon exacto del objeto
     * actual.
     * @return
     */
    public Fecha clone() {
        return new Fecha(this.dia, this.mes, this.anyo);
    }

    /**
     * Devuelve el día de la semana que representa por la Fecha actual
     * @return @dia
     */
    public int getDia() {
        return this.dia;
    }

    /**
     * Devuelve el mes que representa la Fecha actual
     * @return @mes
     */
    public int getMes(){
        return this.mes;
    }

    /**
     * Devuelve el año que representa la Fecha actual
     * @return @mes
     */
    public int getAnyo(){
        return this.anyo;
    }

    /**
     * Muestra por pantalla la fecha en formato español dd-mm-yyyy
     */
    public void mostrarFormatoES()  {
        System.out.printf("%02d-%02d-%04d %n", this.dia, this.mes, this.anyo);
    }

    /**
     * Muestra por pantalla la fecha en formato inglés yyyy-mm-dd
     */
    public void mostrarFormatoGB() {
        System.out.printf("%04d-%02d-%02d %n", this.anyo, this.mes, this.dia);
    }

    /**
     * Muestra por pantalla la fecha en formato texto dd-mmmmm-yyyy
     */
    public void mostrarFormatoTexto() {
        System.out.printf("%s-%s-%04d %n", this.dia, this.getMesTexto(), this.anyo);
    }

    /**
     * Retorna un booleano indicando si la fecha del objeto es igual a la fecha pasada como
     * argumento
     *
     * @return boolean
     */
    public boolean isEqual(Fecha fecha) {
        return (this.dia == fecha.dia)
                && (this.mes == fecha.mes)
                && (this.anyo == fecha.anyo);
    }

    /**
     * Retorna el dia correspondiente de la semana en formato String
     * @return String
     */
    public String getDiaSemana() {
        int diasTranscurridosOrigen = getDiasTranscurridosOrigen();
        return DIAS_TEXTO[diasTranscurridosOrigen % DIAS_POR_SEMANA];
    }

    /**
     * Solo Festivo sábado o domingo
     * @return boolean
     */
    public boolean isFestivo() {
        String diaSemana = getDiaSemana();
        return diaSemana.equals("sabado")
                || diaSemana.equals("domingo");
    }

    public int getNumeroSemana() {
        int numeroSemana = 1;

        for (int i = 1; i <= MESES_POR_ANYO; i++) {
            for (int j = 1; j <= getDiasMes(i, this.anyo) ; j++) {

                Fecha fechaTemp = new Fecha(j, i, this.anyo);
                if(fechaTemp.isEqual(this)){
                    return numeroSemana;
                }

                if(fechaTemp.getDiaSemana().equals("domingo")){
                    numeroSemana++;
                }
            }
        }

        return numeroSemana;

    }

    private Fecha getFechaDias(int cantDias){

        int c = 1;
        for (int i = 1; true ; i++) {
            for (int j = 1; j <= MESES_POR_ANYO ; j++) {
                for (int k = 1; k <= getDiasMes(j, i); k++) {
                    if(c==cantDias){
                        return new Fecha(k, j, i);
                    }
                    c++;
                }
            }
        }
    }


    /**
     * El numéro máximo de dias que podemos añadir es 30
     *
     * @param numDias
     * @return
     */
    public Fecha anyadir(int numDias) {
        Fecha copia = this.clone();
        if(numDias >0 && numDias<=30){
            copia = getFechaDias(copia.getDiasTranscurridosOrigen() + numDias);
        }

        return copia;
    }

    public Fecha restar(int numDias){
        Fecha copia = clone();
        if(numDias >0 && numDias<=30) {
            copia = getFechaDias(copia.getDiasTranscurridosOrigen() - numDias);

        }
        return copia;

    }

    public boolean isCorrecta(){
        return dia <= getDiasMes(mes, anyo) && (mes >= 1 && mes <= MESES_POR_ANYO);
    }

    /**
     * Retorna el dia correspondiente de la semana en formato caracter
     * @return char
     */
    private String getMesTexto() {
        return MESES_TEXTO[mes - 1];
    }

    /**
     * Devuelve el número de dias transcurridos desde el 1-1-1
     *
     * @return int
     */
    private int getDiasTranscurridosOrigen() {
        int dias = 0;
        for (int i = 1; i <= this.anyo-1; i++) {
            dias += getDiasAnyo(i);
        }

        dias += getDiasTranscurridosAnyo();
        return dias;

    }

    /**
     * Devuelve el número de dias transcurridos en el anyo actual
     *
     * @return int
     */
    private int getDiasTranscurridosAnyo() {
        int totalDias = dia;
        for (int i = 1; i < mes; i++){
            totalDias += getDiasMes(i, anyo);
        }
        return totalDias;
    }

    /**
     * Indica si el año pasado como argumento es bisiesto
     * Un año es bisiesto si es divisible por 4 a su vez 100 por 400
     *
     * @return boolean
     */
    private boolean isBisiesto(int anyo){
        if (anyo % 4 == 0) {
            if (anyo % 100 != 0) {
                return true;
            }
            return (anyo % 400 == 0);
        }
        return false;
    }

    /**
     *  Calcula el número de días que tiene el mes representado por la fecha actual
     *
     *  @return int total dias mes en curso
     *
     */
    public int getDiasMes(int mes, int anyo) {

        if( mes == 4 || mes == 6|| mes == 9
                || mes == 11) {
            return 30;
        }else if (mes == 2){
            return  (isBisiesto(anyo)) ? 29: 28;
        }
        return 31;
    }

    /**
     * Calcula el número total de dias que tiene el año pasado como argumento
     *
     * @return int total dias anyo en curso
     */
    private int getDiasAnyo(int anyo){
        return (isBisiesto(anyo)) ? DIAS_POR_ANYO+1 : DIAS_POR_ANYO;
    }

    /**
     * Obtiene el número de años transcurridos entre dos fechas.
     * @param fecha La fecha sobre la que se calculará la diferencia de años
     * @return El número de años de diferencia hasta la fecha proporcionada
     */
    public int getAnyosTranscurridos(Fecha fecha) {
        int anyosTranscurridos = fecha.anyo - (anyo - 1);
        if (mes > fecha.mes
                || mes == fecha.mes && dia <= fecha.dia) {
            anyosTranscurridos++;
        }
        return anyosTranscurridos;
    }


}
