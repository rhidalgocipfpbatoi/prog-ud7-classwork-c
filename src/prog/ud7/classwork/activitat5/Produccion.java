package prog.ud7.classwork.activitat5;

public abstract class Produccion {

    private String title;

    private long duration;

    private String summary;

    private Formato format;

    private Fecha releaseDate;

    public Produccion(String title, long duration, Formato format, Fecha releaseDate) {
        this.title = title;
        this.duration = duration;
        this.format = format;
        this.releaseDate = releaseDate;
    }

    public void showDetails() {
        System.out.printf("%s (%d)%n",  title, releaseDate.getAnyo());
        System.out.println("Descripción: " + summary);
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    protected String getDurationTime() {
        int totalSeconds = (int) duration;
        int horas = totalSeconds / 3600;
        totalSeconds -= horas * 3600;
        int minutes = totalSeconds / 60;
        totalSeconds -= minutes * 60;
        return String.format("%dh %dmin %ds", horas, minutes, totalSeconds);
    }

    @Override
    public String toString() {
        return "Producción: " + "título = " + title + ", duración=" + getDurationTime() + ", formato=" + format;
    }
}
