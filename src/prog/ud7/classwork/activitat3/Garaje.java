/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat3;

import prog.ud7.classwork.activitat3.tiposvehiculos.Coche;
import prog.ud7.classwork.activitat3.tiposvehiculos.CocheDeportivo;

/**
 *
 * @author batoi
 */
public class Garaje {
    public static void main(String[] args) {
        Coche coche = new Coche(12, 25, "111111A");
        CocheDeportivo cocheDeportivo = new CocheDeportivo(false, "222222B");
        
        coche.repostar(50);
        cocheDeportivo.repostar(50);
        
        for (int i = 0; i < 5; i++) {
            coche.acelerar();
            cocheDeportivo.acelerar();
        }
        
        System.out.println(coche);
        System.out.println(cocheDeportivo);
    }
}
