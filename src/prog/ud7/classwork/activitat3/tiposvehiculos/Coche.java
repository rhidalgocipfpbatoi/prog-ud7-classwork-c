/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat3.tiposvehiculos;

/**
 *
 * @author batoi
 */
public class Coche extends Vehiculo{
    
    protected float carburante;
    private String matricula;
    
    public Coche(int velocidad, float carburante, String matricula) {
        super(velocidad);
        this.carburante = carburante;
        this.matricula = matricula;
    }

    @Override
    public void acelerar() {
        super.acelerar(); 
        this.carburante -= 0.5;
    }
    
    public void repostar(int cantidad) {
        this.carburante += cantidad;
    }

    @Override
    public String toString() {
        return "Coche{" + "carburante=" + carburante + '}';
    }
    
    
    
}
