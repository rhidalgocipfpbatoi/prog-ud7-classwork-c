/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat3.tiposvehiculos;

/**
 *
 * @author batoi
 */
public class Vehiculo {
    
    private int velocidad;
    private int ruedas;
    
    public Vehiculo(int velocidad) {
        this.velocidad = velocidad;
        this.ruedas = 4;
    }
    
    public void acelerar() {
        this.velocidad += 10;
    }
    
    public void frenar() {
        this.velocidad = 0;
    }
}
