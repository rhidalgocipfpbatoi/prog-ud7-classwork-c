/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud7.classwork.activitat3.tiposvehiculos;

/**
 *
 * @author batoi
 */
public class CocheDeportivo extends Coche {

    private boolean esDescapotable;
    
    public CocheDeportivo(boolean esDescapotable, String matricula) {
        super(0, 0.5f, matricula);
        this.esDescapotable = esDescapotable;
    }

    @Override
    public void acelerar() {
        super.acelerar(); 
        super.carburante -= 1;
    }
}
