package prog.ud7.classwork.actividad9;

import prog.ud7.classwork.actividad9.enums.Size;
import prog.ud7.classwork.actividad9.types.Animal;
import prog.ud7.classwork.actividad9.types.Cat;
import prog.ud7.classwork.actividad9.types.Dog;
import prog.ud7.classwork.actividad9.types.Hippo;
import prog.ud7.classwork.actividad9.types.Horse;
import prog.ud7.classwork.actividad9.types.Lion;
import prog.ud7.classwork.actividad9.types.Tiger;
import prog.ud7.classwork.actividad9.types.Wolf;

public class TestAnimal {

    public static void main(String[] args) {

        Animal[] animals = new Animal[10];
        animals[0] = new Lion(Size.BIG, "La Sabana");
        animals[1] = new Wolf(Size.BIG, "La Sabana");
        animals[2] = new Wolf(Size.MEDIUM, "La Serreta");
        animals[3] = new Dog(Size.SMALL, "Alcoy");
        animals[4] = new Cat(Size.SMALL, "Spain");
        animals[5] = new Cat(Size.SMALL, "Spain");
        animals[6] = new Horse(Size.BIG, "Spain");
        animals[7] = new Hippo(Size.BIG, "La Sabana");
        animals[8] = new Tiger(Size.BIG, "África");
        animals[9] = new Tiger(Size.BIG, "África");

        System.out.println("---- Antes de vacunarlos y alimentarlos ------");
        for (Animal animal : animals) {
            System.out.println(animal);
        }
        System.out.println();

        Vet vet = new Vet();
        for (Animal animal : animals) {
            vet.vaccinate(animal);
            vet.feed(animal);
        }

        System.out.println();
        System.out.println("---- Después de vacunarlos y alimentarlos ------");
        for (Animal animal : animals) {
            System.out.println(animal);
        }

    }

}
