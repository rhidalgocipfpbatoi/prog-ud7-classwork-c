package prog.ud7.classwork.actividad9;

import prog.ud7.classwork.actividad9.types.Animal;
import prog.ud7.classwork.actividad9.types.Lion;
import prog.ud7.classwork.actividad9.types.Tiger;



public class Vet {

    public void vaccinate(Animal animal) {
        if (animal instanceof Lion
                || animal instanceof Tiger) {
            animal.eat();
        }
        animal.vaccinate();
    }

    public void feed(Animal animal) {
        animal.eat();
    }

}
