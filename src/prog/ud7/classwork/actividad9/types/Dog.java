package prog.ud7.classwork.actividad9.types;

import prog.ud7.classwork.actividad9.enums.Food;
import prog.ud7.classwork.actividad9.enums.Size;

public class Dog extends Animal {

    private static String NOMBRE = "Perro";

    public Dog(Size size, String location) {
        super(Food.OMNIVORE, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("GUAU!!!");
    }
}
