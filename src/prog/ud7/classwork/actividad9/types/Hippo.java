package prog.ud7.classwork.actividad9.types;

import prog.ud7.classwork.actividad9.types.Animal;
import prog.ud7.classwork.actividad9.enums.Food;
import prog.ud7.classwork.actividad9.enums.Size;


public class Hippo extends Animal {

    private static String NOMBRE = "Hipopótamo";

    public Hippo(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("HIPPPP!!!!!!!!!");
    }
}
